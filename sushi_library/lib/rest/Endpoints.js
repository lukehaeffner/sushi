"use strict";

module.exports.BASE_URL = "/api/v6";

module.exports.CHANNELS =    "/channels"; 
module.exports.GATEWAY =     "/gateway";
module.exports.GATEWAY_BOT = "/gateway/bot";